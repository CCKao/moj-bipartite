"use strict";

人數總編號,涉案類別,性別,職務層級,犯罪時服務機關;
1,行政事務,女,中層薦任,地方行政機關;
2,其他,女,基層委任,地方行政機關;
3,衛生醫療,男,高層簡任,中央行政機關;
4,衛生醫療,男,基層委任,中央行政機關;
5,行政事務,女,基層委任,地方行政機關;
6,法務,男,基層委任,中央行政機關;
7,農林漁牧,男,中層薦任,中央行政機關;
8,農林漁牧,男,約聘僱,中央行政機關;
9,農林漁牧,男,基層委任,中央行政機關;
10,農林漁牧,男,中層薦任,中央行政機關;
11,農林漁牧,男,基層委任,中央行政機關;
12,農林漁牧,男,中層薦任,中央行政機關;
13,農林漁牧,男,約聘僱,中央行政機關;
14,農林漁牧,男,基層委任,中央行政機關;
15,農林漁牧,男,中層薦任,中央行政機關;
16,農林漁牧,男,中層薦任,中央行政機關;
17,農林漁牧,男,約聘僱,中央行政機關;
18,農林漁牧,男,中層薦任,中央行政機關;
19,農林漁牧,男,約聘僱,中央行政機關;
20,農林漁牧,男,中層薦任,中央行政機關;
21,農林漁牧,男,中層薦任,中央行政機關;
22,農林漁牧,男,約聘僱,中央行政機關;
23,農林漁牧,男,中層薦任,中央行政機關;
24,農林漁牧,男,約聘僱,中央行政機關;
25,農林漁牧,男,中層薦任,中央行政機關;
26,農林漁牧,男,中層薦任,中央行政機關;
27,農林漁牧,男,中層薦任,中央行政機關;
28,農林漁牧,男,中層薦任,中央行政機關;
29,農林漁牧,男,約聘僱,中央行政機關;
30,農林漁牧,女,約聘僱,中央行政機關;
31,農林漁牧,女,約聘僱,中央行政機關;
32,農林漁牧,女,約聘僱,中央行政機關;
33,農林漁牧,男,中層薦任,中央行政機關;
34,農林漁牧,女,約聘僱,中央行政機關;
35,農林漁牧,女,約聘僱,中央行政機關;
36,農林漁牧,男,基層委任,中央行政機關;
37,農林漁牧,男,基層委任,中央行政機關;
38,農林漁牧,男,基層委任,中央行政機關;
39,農林漁牧,男,基層委任,中央行政機關;
40,農林漁牧,男,約聘僱,中央行政機關;
41,農林漁牧,女,中層薦任,中央行政機關;
42,農林漁牧,男,基層委任,中央行政機關;
43,農林漁牧,男,基層委任,中央行政機關;
44,農林漁牧,男,基層委任,中央行政機關;
45,農林漁牧,女,約聘僱,中央行政機關;
46,農林漁牧,女,中層薦任,中央行政機關;
47,農林漁牧,男,基層委任,中央行政機關;
48,農林漁牧,男,基層委任,中央行政機關;
49,農林漁牧,男,中層薦任,中央行政機關;
50,農林漁牧,男,中層薦任,中央行政機關;
51,農林漁牧,女,中層薦任,中央行政機關;
52,稅務,男,中層薦任,中央行政機關;
53,警政,男,中層薦任,地方行政機關;
54,國家安全情報,男,高層簡任,中央行政機關;
55,國家安全情報,男,中層薦任,中央行政機關;
56,民戶役地政,女,中層薦任,地方行政機關;
57,行政事務,男,中層薦任,地方行政機關;
58,衛生醫療,男,中層薦任,中央行政機關;
59,衛生醫療,男,約聘僱,中央行政機關;
60,其他,男,民意代表,地方民意機關;
61,其他,男,民意代表,地方民意機關;
62,其他,女,民意代表,地方民意機關;
63,其他,男,民意代表,地方民意機關;
64,軍方事務,男,基層委任,中央行政機關;
65,軍方事務,男,基層委任,中央行政機關;
66,民戶役地政,男,基層委任,地方行政機關;
67,司法,男,民意代表,中央行政機關;
68,警政,男,基層委任,地方行政機關;
69,警政,男,基層委任,地方行政機關;
70,警政,男,中層薦任,地方行政機關;
71,警政,男,基層委任,地方行政機關;
72,消防,男,中層薦任,地方行政機關;
73,警政,男,基層委任,中央行政機關;
74,行政事務,女,約聘僱,中央行政機關;
75,其他,男,約聘僱,地方行政機關;
76,其他,女,約聘僱,地方行政機關;
77,行政事務,男,基層委任,地方行政機關;
78,司法,男,基層委任,中央行政機關;
79,營建,男,高層簡任,地方行政機關;
80,營建,男,民意代表,地方行政機關;
81,營建,男,高層簡任,地方行政機關;
82,營建,男,民意代表,地方行政機關;
83,營建,男,中層薦任,地方行政機關;
84,行政事務,男,基層委任,地方行政機關;
85,行政事務,女,基層委任,地方行政機關;
86,行政事務,男,基層委任,地方行政機關;
87,行政事務,男,民意代表,地方民意機關;
88,行政事務,男,約聘僱,地方行政機關;
89,軍方事務,男,中層薦任,中央行政機關;
90,行政事務,女,民意代表,地方民意機關;
91,行政事務,女,民意代表,地方民意機關;
92,農林漁牧,男,中層薦任,地方行政機關;
93,農林漁牧,男,民意代表,地方民意機關;
94,農林漁牧,男,中層薦任,中央行政機關;
95,農林漁牧,男,基層委任,中央行政機關;
96,農林漁牧,男,基層委任,中央行政機關;
97,農林漁牧,男,約聘僱,中央行政機關;
98,行政事務,男,中層薦任,中央行政機關;
99,其他,男,高層簡任,地方行政機關;
100,行政事務,男,中層薦任,地方行政機關;
101,行政事務,女,基層委任,地方行政機關;
102,行政事務,男,中層薦任,地方行政機關;
103,行政事務,男,基層委任,地方行政機關;
104,移民與海岸巡防,男,中層薦任,中央行政機關;
105,移民與海岸巡防,女,基層委任,中央行政機關;
106,警政,男,基層委任,地方行政機關;
107,警政,男,基層委任,地方行政機關;
108,行政事務,女,基層委任,地方行政機關;
109,警政,男,基層委任,地方行政機關;
110,警政,男,中層薦任,地方行政機關;
111,軍方事務,男,基層委任,中央行政機關;
112,農林漁牧,男,中層薦任,地方行政機關;
113,農林漁牧,男,約聘僱,地方行政機關;
114,營建,男,約聘僱,地方行政機關;
115,教育,女,中層薦任,地方行政機關;
116,關務,男,高層簡任,中央行政機關;
117,關務,男,中層薦任,中央行政機關;
118,關務,男,基層委任,中央行政機關;
119,公路監理,女,基層委任,中央行政機關;
120,稅務,男,中層薦任,中央行政機關;
121,國有財產事務,男,中層薦任,地方行政機關;
122,國有財產事務,女,基層委任,地方行政機關;
123,行政事務,男,民意代表,地方民意機關;
124,行政事務,男,中層薦任,中央行政機關;
125,行政事務,男,中層薦任,中央行政機關;
126,環保,男,高層簡任,地方行政機關;
127,警政,男,基層委任,地方行政機關;
128,警政,男,基層委任,地方行政機關;
129,警政,男,基層委任,地方行政機關;
130,民戶役地政,男,中層薦任,地方行政機關;
131,營建,男,高層簡任,地方行政機關;
132,營建,男,民意代表,地方行政機關;
133,營建,女,民意代表,地方民意機關;
134,行政事務,女,民意代表,地方行政機關;
135,行政事務,女,約聘僱,地方行政機關;
136,行政事務,男,基層委任,地方行政機關;
137,營建,男,高層簡任,地方行政機關;
138,法務,男,中層薦任,中央行政機關;
139,法務,男,基層委任,中央行政機關;
140,行政事務,男,民意代表,地方民意機關;
141,行政事務,女,民意代表,地方民意機關;
142,行政事務,男,民意代表,地方民意機關;
143,衛生醫療,男,民意代表,中央行政機關;
144,環保,男,民意代表,地方行政機關;
145,環保,男,民意代表,地方行政機關;
146,環保,男,民意代表,地方行政機關;
147,行政事務,男,約聘僱,地方行政機關;
148,行政事務,男,約聘僱,地方行政機關;
149,警政,男,基層委任,地方行政機關;
150,警政,男,基層委任,地方行政機關;
151,營建,男,基層委任,地方行政機關;
152,行政事務,男,高層簡任,地方行政機關;
153,行政事務,女,約聘僱,中央行政機關;
154,國營事業,男,基層委任,中央行政機關;
155,河川及砂石管理,男,中層薦任,中央行政機關;
156,河川及砂石管理,男,約聘僱,中央行政機關;
157,國營事業,男,中層薦任,中央行政機關;
158,國營事業,男,中層薦任,中央行政機關;
159,警政,男,中層薦任,地方行政機關;
160,行政事務,女,民意代表,地方行政機關;
161,警政,男,基層委任,中央行政機關;
162,環保,男,約聘僱,地方行政機關;
163,行政事務,男,民意代表,地方民意機關;
164,行政事務,女,民意代表,地方民意機關;
165,行政事務,男,中層薦任,地方行政機關;
166,環保,男,約聘僱,地方行政機關;
167,消防,男,基層委任,地方行政機關;
168,消防,女,基層委任,地方行政機關;
169,行政事務,男,民意代表,地方民意機關;
170,環保,男,約聘僱,地方行政機關;
171,行政事務,男,中層薦任,中央行政機關;
172,農林漁牧,男,中層薦任,地方行政機關;
173,農林漁牧,男,中層薦任,地方行政機關;
174,營建,男,約聘僱,中央行政機關;
175,國營事業,男,中層薦任,地方行政機關;
176,國營事業,男,中層薦任,地方行政機關;
177,運輸觀光氣象,男,基層委任,中央行政機關;
178,法務,男,基層委任,中央行政機關;
179,軍方事務,男,基層委任,中央行政機關;
180,警政,男,基層委任,地方行政機關;
181,民戶役地政,男,中層薦任,地方行政機關;
182,營建,男,高層簡任,地方行政機關;
183,營建,男,民意代表,中央行政機關;
184,營建,女,民意代表,中央行政機關;
185,營建,男,民意代表,中央行政機關;
186,營建,男,民意代表,中央行政機關;
187,營建,男,民意代表,中央行政機關;
188,營建,男,民意代表,地方民意機關;
189,營建,男,中層薦任,地方行政機關;
190,營建,男,民意代表,地方行政機關;
191,營建,男,高層簡任,中央行政機關;
192,營建,男,中層薦任,地方行政機關;
193,其他,男,民意代表,地方民意機關;
194,警政,男,基層委任,地方行政機關;
195,警政,男,基層委任,地方行政機關;
196,行政事務,男,民意代表,地方行政機關;
197,行政事務,男,民意代表,地方行政機關;
198,行政事務,男,民意代表,地方行政機關;
199,行政事務,男,民意代表,地方行政機關;
200,教育,男,中層薦任,地方行政機關;
201,教育,男,中層薦任,地方行政機關;
202,國有財產事務,男,高層簡任,地方行政機關;
203,行政事務,女,中層薦任,地方行政機關;
204,警政,男,基層委任,地方行政機關;
205,軍方事務,男,中層薦任,中央行政機關;
206,行政事務,女,約聘僱,地方行政機關;
207,民戶役地政,女,高層簡任,地方行政機關;
208,民戶役地政,男,中層薦任,地方行政機關;
209,民戶役地政,男,約聘僱,地方行政機關;
210,其他,男,民意代表,地方行政機關;
211,行政事務,男,民意代表,地方民意機關;
212,行政事務,男,民意代表,地方民意機關;
213,警政,男,基層委任,地方行政機關;
214,警政,男,基層委任,地方行政機關;
215,行政事務,男,中層薦任,地方行政機關;
216,工商監督管理,男,中層薦任,中央行政機關;
217,工商監督管理,男,約聘僱,中央行政機關;
218,工商監督管理,男,約聘僱,中央行政機關;
219,行政事務,男,中層薦任,地方行政機關;
220,警政,男,中層薦任,地方行政機關;
221,國有財產事務,男,中層薦任,地方行政機關;
222,行政事務,男,中層薦任,地方行政機關;
223,行政事務,女,中層薦任,中央行政機關;
224,行政事務,男,民意代表,地方行政機關;
225,國有財產管理,男,中層薦任,中央行政機關;
226,衛生醫療,男,高層簡任,地方行政機關;
227,衛生醫療,男,高層簡任,地方行政機關;
228,環保,男,基層委任,地方行政機關;
229,警政,男,基層委任,地方行政機關;
230,農林漁牧,男,約聘僱,地方行政機關;
231,行政事務,男,基層委任,中央行政機關;
232,行政事務,男,中層薦任,地方行政機關;
233,行政事務,男,基層委任,地方行政機關;
234,農林漁牧,男,約聘僱,地方行政機關;
235,行政事務,女,民意代表,地方民意機關;
236,警政,男,中層薦任,地方行政機關;
237,警政,男,中層薦任,地方行政機關;
238,警政,男,中層薦任,地方行政機關;
239,行政事務,男,中層薦任,地方行政機關;
240,行政事務,男,高層簡任,中央行政機關;
241,行政事務,男,基層委任,中央行政機關;
242,行政事務,男,基層委任,地方行政機關;
243,行政事務,男,中層薦任,中央行政機關;
244,行政事務,男,中層薦任,地方行政機關;
245,行政事務,男,中層薦任,地方行政機關;
